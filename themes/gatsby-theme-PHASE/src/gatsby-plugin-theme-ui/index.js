import { keyframes } from "@emotion/react"
const breakpoints = [90]

const lightup = keyframes({
  "0%": {
    fill: "rgba(255, 255, 255, 1)",
  },
  "12%": {
    fill: "rgba(255, 255, 255, 0.89)",
  },
  "24%": {
    fill: "rgba(255, 255, 255, 0.56)",
  },
  "36%": {
    fill: "rgba(255, 255, 255, 0.02)",
  },
  "54%": {
    fill: "rgba(255, 255, 255, 0.25)",
  },
  "74%": {
    fill: "rgba(255, 255, 255, 0.02)",
  },
  "82%": {
    fill: "rgba(255, 255, 255, 0.06)",
  },
  "92%": {
    fill: "rgba(255, 255, 255, 0.01)",
  },
  "96%": {
    fill: "rgba(255, 255, 255, 0.02)",
  },
  "100%": {
    fill: "rgba(255, 255, 255, 0)",
  },
})

const theme = {
  breakpoints: breakpoints.map(n => n + "em"),
  space: [0, 10, 20, 24, 32, 50, 48, 60, 56, 16],
  sizes: {
    modal: "540px",
    container: "90em",
  },
  colors: {
    primary: "black",
    text: "#ffffff",
    background: "#212121",
  },
  fonts: {
    body: "area",
    heading: "drukwide",
  },
  fontWeights: {
    body: 600,
    heading: 600,
    bold: 1000,
  },
  fontSizes: ["1rem", "1.5rem", "1.25rem"],
  lineHeights: {
    body: 1.625,
    heading: 1,
    bold: 1.5,
  },
  zIndices: {
    loading: 9999,
  },
  text: {
    p: {
      fontFamily: "body",
      fontWeight: "body",
      lineHeight: "body",
      letterSpacing: "-0.04em",
      fontSize: 0,
    },
    bold: {
      variant: "text.p",
      fontSize: 2,
    },
    heading: {
      fontFamily: "heading",
      fontWeight: "heading",
      lineHeight: "heading",
      letterSpacing: "-0.02em",
      textTransform: "uppercase",
    },
    h1: {
      variant: "text.heading",
      fontSize: ["40px", "112px"],
    },
    h2: {
      variant: "text.heading",
      fontSize: ["27px", "90px"],
    },
    h3: {
      variant: "text.heading",
      fontSize: ["20px", "54px"],
    },
    h4: {
      variant: "text.p",
      fontSize: 2,
    },
    highlight: {
      variant: "text.h1",
      fontSize: ["36px", "112px"],
    },
  },
  buttons: {
    primary: {
      variant: "text.p",
      display: "inline-block",
      color: "background",
      bg: "text",
      p: 3,
      borderRadius: 0,
      cursor: "pointer",
      textAlign: "center",
    },
    secondary: {
      variant: "buttons.primary",
      color: "text",
      bg: "background",
    },
    close: {
      variant: "buttons.primary",
      position: "absolute",
      top: 0,
      right: 0,
      backgroundColor: "transparent",
      p: 9,
    },
  },
  forms: {
    checkbox: {
      borderRadius: 0,
      borderColor: "black",
      m: 0,
      mr: 1,
    },
    input: {
      borderRadius: 0,
      borderColor: "black",
      p: "12px 15px",
      outline: "none",
      mb: 9,
      "&.is-error": {
        borderColor: "#be2f2f",
      },
    },
  },
  layout: {
    jumbotron: {
      maxWidth: "none",
      position: "relative",
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      justifyContent: "center",
      rowGap: 1,
      mt: [5, 7],
      py: ["30px", "100px"],
      textAlign: "center",
      minHeight: "80vh",
      mb: "180px",
      p: {
        px: 5,
      },
    },
    modal: {
      maxWidth: "none",
      position: "fixed",
      top: 0,
      left: 0,
      right: 0,
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      minHeight: "100vh",
      backgroundColor: "rgba(0, 0 , 0, .8)",
      p: 4,
      opacity: 0,
      pointerEvents: "none",
      zIndex: 100,
      transition: "opacity 300ms ease-out",
      transitionDelay: "300ms",
      "> *": {
        position: "relative",
        flexGrow: 1,
        backgroundColor: "text",
        color: "background",
        p: [4, 8],
        maxWidth: "modal",
        transform: "translateY(10px)",
        transitionDelay: 0,
        transition: "all 200ms ease-out",
        opacity: 0,
      },
      "&.is-active": {
        opacity: 1,
        pointerEvents: "auto",
        transitionDelay: "0ms",
        transition: "opacity 300ms ease-in",
        "> *": {
          transitionDelay: "300ms",
          transition: "all 200ms ease-in",
          transform: "translateY(0)",
          opacity: 1,
        },
      },
    },
    adress: {
      position: "relative",
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      textAlign: "center",
      rowGap: [1, 2],
      maxWidth: ["420px", "90em"],
      pt: ["50px", "113px"],
      pb: ["94px", "264px"],
      mb: ["180px", "260px"],
      px: [9, 6],
      ".gatsby-image-wrapper": {
        position: "absolute",
        zIndex: -1,
        "&:first-of-type": {
          left: [9, "85px"],
          top: 0,
          width: ["146px", "410px"],
          height: ["167px", "auto"],
        },
        "&:nth-of-type(2)": {
          right: [9, 0],
          top: ["130px", "160px"],
          width: ["179px", "664px"],
          height: ["144px", "auto"],
        },
        "&:last-of-type": {
          left: ["56px", "340px"],
          bottom: 0,
          width: ["177px", "490px"],
        },
      },
    },
    highlight: {
      maxWidth: "none",
      position: "relative",
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      height: "100vh",
      py: [5, 7],
      textAlign: "center",
      h1: {
        maxWidth: ["420px", "none"],
      },
      ".gatsby-image-wrapper": {
        position: "absolute",
        top: [5, 7],
        left: 0,
        right: 0,
        bottom: [5, 7],
        zIndex: -1,
      },
    },
    text: {
      display: "flex",
      flexDirection: ["column", "row"],
      alignItems: "center",
      px: 6,
      "> *": {
        width: ["280px", "50%"],
      },
      columnGap: "90px",
      mb: "140px",
    },
    directors: {
      position: "relative",
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      textAlign: "center",
      rowGap: 5,
      pt: [0, "186px"],
      pb: [0, "186px"],
      mb: "140px",
    },
    footer: {
      display: "flex",
      flexDirection: ["column", "row"],
      alignItems: "center",
      justifyContent: "center",
      textAlign: "center",
      rowGap: 9,
      px: [3],
      pb: ["150px"],
      hr: {
        display: ["none", "initial"],
        mx: 2,
        my: 0,
        width: "1px",
        border: "none",
        height: "30px",
        background: "text",
      },
    },
  },
  images: {
    icon: {
      display: "inline-block",
      verticalAlign: "middle",
      "& + *": {
        verticalAlign: "middle",
      },
    },
  },
  variants: {
    loading: {
      position: "fixed",
      top: 0,
      right: 0,
      bottom: 0,
      left: 0,

      display: "flex",
      justifyContent: "center",
      alignItems: "center",

      width: "100%",
      height: "100%",

      zIndex: "loading",
      opacity: 1,

      background: "background",

      pointerEvents: "none",
    },
    background: {
      position: "absolute",
      zIndex: -1,
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      px: [4, 6],
      ".gatsby-image-wrapper": {
        width: "100%",
        height: "100%",
      },
    },
    lightup: {
      animationDuration: "500ms",
      animationDirection: "reverse",
      animationFillMode: "forwards",
      animationDelay: "200ms",
      animationName: `${lightup}`,
    },
  },
  styles: {
    root: {
      fontFamily: "body",
      body: {},
      ".is-outline": {
        color: "transparent",
        WebkitTextStrokeWidth: "1px",
        WebkitTextStrokeColor: "#fff",
      },
      ".is-desktop": {
        display: ["none", "initial"],
      },
      ".is-mobile": {
        display: ["initial", "none"],
      },
      "*::-webkit-scrollbar": {
        width: "scrollbar",
        background: "#181818",
      },
      "*::-webkit-scrollbar-thumb": {
        background: "text",
      },
      ".gatsby-image-wrapper [data-main-image]": {
        opacity: "1 !important",
        transition: "none !important",
      },
    },
  },
}

export default theme
