import React, { FunctionComponent } from "react"
import { PageProps } from "gatsby"
import { LoadingProvider } from "./components/Loading"
import createLazyContext from "./contexts/createLazyContext"

// Context
export const [useLibsContext, LibsProvider] = createLazyContext<{
  anime: any
  fontfaceobserver: any
}>()

// Componenet
interface OwnProps {}

type Props = PageProps<OwnProps>

const WrapRoot: FunctionComponent<Props> = props => {
  return (
    <LibsProvider>
      <LoadingProvider loading={true}>{props.children}</LoadingProvider>
    </LibsProvider>
  )
}

export default WrapRoot
