import React, { FunctionComponent } from "react"
import { graphql, useStaticQuery } from "gatsby"
import { GatsbyImage, getImage } from "gatsby-plugin-image"
import { Container, Heading, Text } from "theme-ui"

interface OwnProps {}

type Props = OwnProps

const Adress: FunctionComponent<Props> = props => {
  const data = useStaticQuery(graphql`
    query {
      allFile(
        filter: {
          sourceInstanceName: { eq: "images" }
          name: { in: ["adress-1", "adress-2", "adress-3"] }
        }
      ) {
        nodes {
          name
          childImageSharp {
            gatsbyImageData(transformOptions: { fit: COVER }, placeholder: NONE)
          }
        }
      }
    }
  `)

  const images = data.allFile.nodes
    .map((n: any) => ({
      name: n.name,
      image: getImage(n),
    }))
    .reduce((a: any, i: any) => ({ ...a, [i.name]: i.image }), {})

  return (
    <Container variant="adress" as="section">
      <Heading variant="h2" as="h2" className="is-outline">
        Accès
      </Heading>
      <Text variant="h2" as="h2">
        Ciel Rooftop
        <br className="is-desktop" />
        17 Rue Haxo,
        <br className="is-desktop" />
        13001 Marseille
        <br className="is-desktop" /> à partir de 19h
      </Text>
      <Heading variant="h4" as="p">
        Métro 1 Vieux-Port / Tramway 2 Canebière-Capucins / Tramway 3 Rome-Davso
        <br className="is-desktop" />
        Parking Indigo à proximité
      </Heading>
      <GatsbyImage image={images["adress-1"]} alt="PHA5E STUDIO" />
      <GatsbyImage image={images["adress-2"]} alt="PHA5E STUDIO" />
      <GatsbyImage image={images["adress-3"]} alt="PHA5E STUDIO" />
    </Container>
  )
}

export default Adress
