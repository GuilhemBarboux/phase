import React, { FunctionComponent, PropsWithChildren } from "react"
import { Box } from "theme-ui"
import Footer from "./Footer"
import { usePageContext } from "../WrapPage"

interface OwnProps {}

type Props = PropsWithChildren<OwnProps>

const Layout: FunctionComponent<Props> = props => {
  const { ready } = usePageContext()

  return (
    <Box
      sx={{
        height: "100vh",
        overflowX: "hidden",
        overflowY: "auto",
        // overflowY: ready ? "auto" : "hidden",
      }}
    >
      {props.children}
      <Footer />
    </Box>
  )
}

export default Layout
