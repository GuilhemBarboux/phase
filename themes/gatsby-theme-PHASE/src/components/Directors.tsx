import React, { FunctionComponent } from "react"
import { Container, Flex, Heading, Text } from "theme-ui"
import { graphql, useStaticQuery } from "gatsby"
import { GatsbyImage, getImage } from "gatsby-plugin-image"

interface OwnProps {}

type Props = OwnProps

const Directors: FunctionComponent<Props> = props => {
  const data = useStaticQuery(graphql`
    query {
      allFile(
        filter: {
          sourceInstanceName: { eq: "images" }
          name: { in: ["director1", "director2"] }
        }
      ) {
        nodes {
          name
          childImageSharp {
            gatsbyImageData(
              transformOptions: { fit: INSIDE }
              placeholder: NONE
            )
          }
        }
      }
    }
  `)

  const images = data.allFile.nodes
    .map(n => ({
      name: n.name,
      image: getImage(n),
    }))
    .reduce((a, i) => ({ ...a, [i.name]: i.image }), {})

  return (
    <Container as="section" variant="directors">
      <Heading as="h1" variant={"h1"}>
        qui
        <br />
        sommes
        <br />
        nous?
      </Heading>
      <Text as="p" variant="p" sx={{ maxWidth: ["280px", "664px"] }}>
        PHA5E, studio de créations digitales marseillais, accompagne depuis 8
        ans, les entreprises dans la valorisation de leur savoir-faire au
        travers d’expériences digitales uniques. Plaçant l’émotion au coeur de
        ses objectifs, le studio oeuvrent à créer une expérience originale pour
        les utilisateurs dans chacune de ses réalisations.
        <br />
        <br />
        Nous concevons et réalisons des installations interactives, des
        expériences immersives en AR, VR et des sites interactifs.
      </Text>
      <Flex
        sx={{
          position: "absolute",
          top: 0,
          left: "-5%",
          right: "-5%",
          opacity: 0.8,
          zIndex: -1,
          mixBlendMode: "lighten",
          alignItems: "center",
          justifyContent: "center",
          "> *": {
            width: ["400px", "50%"],
            transform: ["translateX(20%)", "none"],
          },
        }}
      >
        <GatsbyImage
          image={images["director1"]}
          alt="PHA5E STUDIO"
          objectFit={"contain"}
        />
        <GatsbyImage
          className="is-desktop"
          image={images["director2"]}
          alt="PHA5E STUDIO"
          objectFit={"contain"}
        />
      </Flex>
    </Container>
  )
}

export default Directors
