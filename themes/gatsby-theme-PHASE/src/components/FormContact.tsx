import React, { FunctionComponent, useState } from "react"
import { Input, Checkbox, Text, Container, Button, Flex, Image } from "theme-ui"
import { useForm } from "react-hook-form"
import arrowSrc from "../assets/images/arrow-white.png"

interface OwnProps {}

type Props = OwnProps

const FormContact: FunctionComponent<Props> = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm()

  const [gowith, setGowith] = useState(false)
  const [isSend, setIsSend] = useState(false)
  const [data, setData] = useState(false)

  const onSubmit = data => {
    setData(data)
    setIsSend(true)
  }

  const emailRegex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g

  return (
    <Container variant="form" as="form" onSubmit={handleSubmit(onSubmit)}>
      {!isSend ? (
        <>
          <Text
            as="p"
            variant="bold"
            sx={{
              textAlign: "center",
              mb: [3, 4],
            }}
          >
            Je souhaite venir à la soirée “Explore Marseille” le 06/10 au Ciel
            Rooftop.
          </Text>
          <Input
            className={errors["p1-lastname"] ? "is-error" : ""}
            {...register("p1-lastname", { required: true })}
            aria-placeholder="Nom"
            placeholder="Nom"
          />
          <Input
            className={errors["p1-firstname"] ? "is-error" : ""}
            {...register("p1-firstname", { required: true })}
            aria-placeholder="Prénom"
            placeholder="Prénom"
          />
          <Input
            className={errors["p1-email"] ? "is-error" : ""}
            {...register("p1-email", { required: true, pattern: emailRegex })}
            aria-placeholder="Email"
            placeholder="Email"
          />
          <Input
            className="is-desktop"
            {...register("p1-tel", { required: false })}
            aria-placeholder="Téléphone (optionnel)"
            placeholder="Téléphone (optionnel)"
          />
          <Flex
            sx={{
              mb: 4,
              mt: 1,
            }}
          >
            <Checkbox checked={gowith} onChange={() => setGowith(p => !p)} />
            <Text as="span" sx={{ pt: ".32em" }}>
              Je viendrais accompagné
            </Text>
          </Flex>
          {gowith && (
            <>
              <Input
                className={errors["p2-lastname"] ? "is-error" : ""}
                {...register("p2-lastname", { required: gowith })}
                aria-placeholder="Nom"
                placeholder="Nom"
              />
              <Input
                className={errors["p2-firstname"] ? "is-error" : ""}
                {...register("p2-firstname", { required: gowith })}
                aria-placeholder="Prénom"
                placeholder="Prénom"
              />
              <Input
                className={errors["p2-email"] ? "is-error" : ""}
                {...register("p2-email", {
                  required: gowith,
                  pattern: emailRegex,
                })}
                aria-placeholder="Email"
                placeholder="Email"
              />
              <Input
                className={"is-desktop"}
                {...register("p2-tel", { required: false })}
                aria-placeholder="Téléphone (optionnel)"
                placeholder="Téléphone (optionnel)"
              />
            </>
          )}
          <Button
            type="submit"
            variant="secondary"
            onClick={() => {}}
            sx={{
              width: "100%",
            }}
          >
            <Image
              src={arrowSrc}
              sx={{ pr: 1, pb: "3px", verticalAlign: "middle" }}
            />
            <span>Confirmer votre présence</span>
          </Button>
        </>
      ) : (
        <Text as="p" variant="bold">
          Merci {data["p1-firstname"]}, votre présence à bien été confirmée.
        </Text>
      )}
    </Container>
  )
}

export default FormContact
