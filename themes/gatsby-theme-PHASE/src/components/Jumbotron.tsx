import React, { FunctionComponent, useEffect, useRef } from "react"
import { GatsbyImage, getImage } from "gatsby-plugin-image"
import { Button, Box, Container, Heading, Text, Image } from "theme-ui"
import Logo from "./Logo"
import { graphql, useStaticQuery } from "gatsby"
import Icon from "./Icon"
import { useLibsContext } from "../WrapRoot"
import { usePageContext } from "../WrapPage"

interface OwnProps {
  onClick: () => void
}

type Props = OwnProps

const StringToSpan: FunctionComponent<{ text: string }> = ({ text }) => (
  <>
    {text.split("").map((l: string, i: number) => (
      <span key={i}>{l}</span>
    ))}
  </>
)

const Jumbotron: FunctionComponent<Props> = props => {
  const { ready } = usePageContext()
  const { anime } = useLibsContext()
  const animation = useRef<any>(null)
  const jumbotron = useRef<HTMLDivElement>(null!)
  const data = useStaticQuery(graphql`
    query {
      file(sourceInstanceName: { eq: "images" }, name: { eq: "jumbotron" }) {
        childImageSharp {
          gatsbyImageData(transformOptions: { fit: COVER }, placeholder: NONE)
        }
      }
    }
  `)

  const image = getImage(data.file)

  useEffect(() => {
    if (anime) {
      animation.current = anime
        .timeline({
          autoplay: false,
        })
        .add({
          targets: jumbotron.current.querySelectorAll(".jumbotron-background"),
          easing: "easeOutQuad",
          translateX: (el: any, i: number) => {
            return i <= 0 ? [0, "-110%"] : [0, "110%"]
          },
          skewX: ["33deg", "33deg"],
          duration: 2000,
        })
        .add(
          {
            targets: jumbotron.current.querySelector(".jumbotron-logo"),
            opacity: [0, 1],
            translateY: ["50%", "-50%"],
            translateX: ["-50%", "-50%"],
            easing: "easeOutQuad",
            duration: 400,
          },
          "-=1200"
        )
        .add(
          {
            targets: jumbotron.current.querySelectorAll(
              ".jumbotron-title span"
            ),
            easing: "easeInSine",
            duration: 360,
            opacity: [0, 1],
            delay: (el: any, i: number) => {
              return (i % 2 > 0 ? 30 : 0) + i * 25 + Math.random() * 25 * i
            },
          },
          "-=1000"
        )
        .add(
          {
            targets: jumbotron.current.querySelector(".jumbotron-date"),
            easing: "easeOutCubic",
            opacity: [0, 1],
            translateY: ["-50px", 0],
            duration: 280,
          },
          "+=280"
        )
        .add(
          {
            targets: jumbotron.current.querySelector(".jumbotron-subtitle"),
            easing: "easeOutCubic",
            opacity: [0, 1],
            translateY: ["50px", 0],
            duration: 280,
          },
          "+=100"
        )
        .add(
          {
            targets: jumbotron.current.querySelector(".jumbotron-desc"),
            easing: "easeInQuad",
            opacity: [0, 1],
            // translateY: ["8px", 0],
            duration: 480,
          },
          "+=200"
        )
        .add(
          {
            targets: jumbotron.current.querySelector(".jumbotron-button"),
            easing: "easeInQuad",
            opacity: [0, 1],
            // translateY: ["8px", 0],
            duration: 480,
          },
          "-=480"
        )
        .add({
          targets: jumbotron.current.querySelector(".jumbotron-icon"),
          easing: "easeOutCubic",
          opacity: [0, 1],
          // translateY: ["8px", 0],
          duration: 280,
        })
    }
  }, [anime])

  useEffect(() => {
    if (ready && animation.current) {
      console.log("PLAY")
      animation.current.play()
    }
  }, [ready])

  return (
    <Container variant="jumbotron" as="section" ref={jumbotron}>
      <Heading className="jumbotron-date" variant="h3" as="h3">
        06/10 — 19h
      </Heading>
      <Heading className="jumbotron-title" variant="h1" as="h1">
        <StringToSpan text="explore" />
        <br />
        <StringToSpan text="marseille" />
      </Heading>
      <Heading className="jumbotron-subtitle is-outline" variant="h1" as="h1">
        <StringToSpan text="by pha5e" />
      </Heading>
      <Text className="jumbotron-desc" variant="bold" as="p">
        Venez redécouvrir Marseille autrement autour d’un cocktail dînatoire
        <br className="is-desktop" /> en notre compagnie le 6 octobre 2022 à 19h
        au Ciel Rooftop.
        <br className="is-desktop" /> lorem ipsum dolor sit amet lorem ipsum
      </Text>
      <Button className="jumbotron-button" onClick={() => props.onClick()}>
        <Icon sx={{ width: "26px", mr: 1 }} as="span" icon="arrow" />
        viens à la soirée{" "}
        <Text as="span" sx={{ display: ["none", "inline"] }}>
          (seul ou a deux)
        </Text>
      </Button>
      <Icon
        className="jumbotron-icon"
        sx={{ width: "32px", mt: 1 }}
        icon="scroll"
      />
      {image && (
        <Box variant="background">
          <GatsbyImage image={image} alt="PHA5E STUDIO" loading="eager" />
          {[1, 2].map(v => (
            <Box
              className="jumbotron-background"
              key={v}
              sx={{
                position: "absolute",
                top: 0,
                left: v === 1 ? ["-60%", "-45%"] : ["45%", "45%"],
                width: ["150%", "100%"],
                height: "100%",
                background: "background",
              }}
            />
          ))}
        </Box>
      )}
      <Box
        className="jumbotron-logo"
        sx={{
          width: "140px",
          position: "absolute",
          top: 0,
          left: "50%",
        }}
      >
        <Logo />
      </Box>
      <button
        onClick={() => animation.current.play()}
        style={{
          position: "fixed",
          top: 0,
          left: 0,
          zIndex: 1000,
        }}
      >
        replay
      </button>
    </Container>
  )
}

export default Jumbotron
