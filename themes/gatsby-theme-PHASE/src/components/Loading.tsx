import React, { FunctionComponent, useEffect, useState } from "react"
import { AspectRatio, Box, Heading } from "theme-ui"
import createLoadingContext from "../contexts/createLoadingContext"

// Context
export const [useLoading, LoadingProvider] = createLoadingContext()

// Component
interface OwnProps {
  finish: () => void
}

type Props = OwnProps

const Loading: FunctionComponent<Props> = props => {
  const { loading, loaded, percent } = useLoading()
  const [unmount, setUnmount] = useState(false)
  const [isActive, setIsActive] = useState(true)

  useEffect(() => {
    if (loaded) {
      setTimeout(() => {
        setIsActive(false)
        setTimeout(() => {
          setUnmount(true)
          props.finish()
        }, 2000)
      }, 950)
    }
  }, [loading, loaded, setIsActive, setUnmount, props.finish])

  return !unmount ? (
    <Box
      id="loading"
      variant="loading"
      className={isActive ? "is-active" : "is-finish"}
      sx={{
        opacity: 1,
        "&.is-finish": {
          opacity: 0,
          transition: "opacity 400ms ease-out",
          transitionDelay: "1600ms",
          "> *": {
            opacity: 0,
            transition: "all 400ms",
            transitionDelay: "1000ms",
            transform: "translateY(50px)",
          },
        },
        "> *": {
          position: "relative",
          width: ["300px", "560px"],
        },
      }}
    >
      <AspectRatio ratio={221 / 32} sx={{}}>
        <svg
          viewBox="0 0 221 32"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M15.8236 22.4802V30.6226H0.585449V0.792725H30.2152C41.7593 0.792725 44.853 5.36421 44.853 10.9823V12.029C44.853 17.6471 41.6053 22.4956 30.2152 22.4956H15.8236V22.4802ZM15.8236 13.4604H26.4903C28.5682 13.4604 29.1839 12.5369 29.1839 11.5364V11.4595C29.1839 10.382 28.5836 9.53546 26.4903 9.53546H15.8236V13.4604Z"
            stroke="white"
          />
          <path
            d="M46.2845 0.792725H61.5227V10.6899H77.4843V0.792725H92.7224V30.6226H77.4843V20.0021H61.5227V30.6226H46.2845V0.792725Z"
            stroke="white"
          />
          <path
            d="M125.631 27.0517H110.716L109.146 30.6226H93.7537L107.822 0.792725H129.232L144.194 30.6226H127.201L125.631 27.0517ZM118.089 9.9818L114.518 18.2474H121.736L118.089 9.9818Z"
            stroke="white"
          />
          <path
            d="M162.202 31.2229C142.516 31.2229 142.239 24.0502 142.239 21.0795V20.8332H157.831C157.954 21.1565 158.431 22.9112 163.203 22.9112C166.851 22.9112 168.097 21.6336 168.097 20.0174V19.9405C168.097 18.4628 166.851 17.1391 163.203 17.1391C158.508 17.1391 157.908 19.1093 157.831 19.3864H142.67V0.777344H181.719V8.5965H157.985V12.5215C158.739 11.9982 161.71 10.1203 168.374 10.1203H168.698C181.088 10.1203 183.813 14.5687 183.813 19.7866V20.0328C183.813 25.1584 180.965 31.2229 164.203 31.2229H162.202Z"
            stroke="white"
          />
          <path
            d="M220.831 0.792725V9.13525H199.867V11.4595H220.6V19.6327H199.867V22.2031H221V30.6226H184.875V0.792725H220.831Z"
            stroke="white"
          />
        </svg>
        <Box
          sx={{
            position: "absolute",
            top: 0,
            left: 0,
            whiteSpace: "nowrap",
            width: `${percent * 100}%`,
            transition: "width 1200ms ease-in",
            overflow: "hidden",
            ".light": {
              variant: "variants.lightup",
            },
          }}
        >
          <Box
            as="svg"
            viewBox="0 0 221 32"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            sx={{
              width: ["300px", "560px"],
            }}
          >
            <path
              d="M15.8236 22.4802V30.6226H0.585449V0.792725H30.2152C41.7593 0.792725 44.853 5.36421 44.853 10.9823V12.029C44.853 17.6471 41.6053 22.4956 30.2152 22.4956H15.8236V22.4802ZM15.8236 13.4604H26.4903C28.5682 13.4604 29.1839 12.5369 29.1839 11.5364V11.4595C29.1839 10.382 28.5836 9.53546 26.4903 9.53546H15.8236V13.4604Z"
              fill="white"
            />
            <path
              d="M46.2845 0.792725H61.5227V10.6899H77.4843V0.792725H92.7224V30.6226H77.4843V20.0021H61.5227V30.6226H46.2845V0.792725Z"
              fill="white"
            />
            <path
              d="M125.631 27.0517H110.716L109.146 30.6226H93.7537L107.822 0.792725H129.232L144.194 30.6226H127.201L125.631 27.0517ZM118.089 9.9818L114.518 18.2474H121.736L118.089 9.9818Z"
              fill="white"
            />
            <path
              className={isActive ? "" : "light"}
              d="M162.202 31.2229C142.516 31.2229 142.239 24.0502 142.239 21.0795V20.8332H157.831C157.954 21.1565 158.431 22.9112 163.203 22.9112C166.851 22.9112 168.097 21.6336 168.097 20.0174V19.9405C168.097 18.4628 166.851 17.1391 163.203 17.1391C158.508 17.1391 157.908 19.1093 157.831 19.3864H142.67V0.777344H181.719V8.5965H157.985V12.5215C158.739 11.9982 161.71 10.1203 168.374 10.1203H168.698C181.088 10.1203 183.813 14.5687 183.813 19.7866V20.0328C183.813 25.1584 180.965 31.2229 164.203 31.2229H162.202Z"
              stroke={"white"}
            />
            <path
              d="M220.831 0.792725V9.13525H199.867V11.4595H220.6V19.6327H199.867V22.2031H221V30.6226H184.875V0.792725H220.831Z"
              fill="white"
            />
          </Box>
        </Box>
      </AspectRatio>
    </Box>
  ) : null
}

export default Loading
