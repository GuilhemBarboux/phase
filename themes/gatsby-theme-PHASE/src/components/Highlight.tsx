import React, { FunctionComponent } from "react"
import { GatsbyImage, getImage } from "gatsby-plugin-image"
import { Container, Flex, Text } from "theme-ui"
import { graphql, useStaticQuery } from "gatsby"

interface OwnProps {}

type Props = OwnProps

const Highlight: FunctionComponent<Props> = () => {
  const data = useStaticQuery(graphql`
    query {
      file(sourceInstanceName: { eq: "images" }, name: { eq: "jumbotron" }) {
        childImageSharp {
          gatsbyImageData(transformOptions: { fit: COVER }, placeholder: NONE)
        }
      }
    }
  `)

  const image = getImage(data.file)

  return (
    <Container variant="highlight" as="section">
      <Text variant="highlight" as="h1">
        Prêt à <br className="is-desktop" /> découvrir nos{" "}
        <br className="is-desktop" /> INSTAL
        <br className="is-mobile" />
        <span className="is-mobile">-</span>LATIONS{" "}
        <br className="is-desktop" />
        DIGITALES ?
      </Text>
      <GatsbyImage image={image} alt="PHA5E STUDIO" />
    </Container>
  )
}

export default Highlight
