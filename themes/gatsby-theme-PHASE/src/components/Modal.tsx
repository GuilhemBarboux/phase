import React, { FunctionComponent, PropsWithChildren } from "react"
import { Box, Button, Container, Flex } from "theme-ui"

interface OwnProps {
  active: boolean
  onClose: () => void
}

type Props = PropsWithChildren<OwnProps>

const Modal: FunctionComponent<Props> = props => (
  <Container variant="modal" className={props.active ? "is-active" : ""}>
    <Box>
      <Button
        variant="close"
        onClick={e => {
          props.onClose()
          e.stopPropagation()
        }}
      >
        <svg
          width="14"
          height="14"
          viewBox="0 0 14 14"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <rect
            width="18.8811"
            height="0.917883"
            transform="matrix(0.707107 0.707106 -0.707107 0.707106 0.649048 0)"
            fill="#121212"
          />
          <rect
            width="18.8811"
            height="0.917883"
            transform="matrix(0.707107 -0.707106 0.707107 0.707106 0 13.3508)"
            fill="#121212"
          />
        </svg>
      </Button>
      {props.children}
    </Box>
  </Container>
)

export default Modal
