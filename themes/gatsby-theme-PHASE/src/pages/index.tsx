import React, { FunctionComponent, useCallback, useMemo } from "react"
import { Container, Flex, Text } from "theme-ui"
import Layout from "../components/Layout"
import Jumbotron from "../components/Jumbotron"
import { navigate, PageProps } from "gatsby"
import FormContact from "../components/FormContact"
import Modal from "../components/Modal"
import Highlight from "../components/Highlight"
import Adress from "../components/Adress"
import Directors from "../components/Directors"

interface OwnProps {}

type Props = PageProps<OwnProps>

const Home: FunctionComponent<Props> = props => {
  const isContact = useMemo(
    () => props.location.hash === "#contact",
    [props.location.hash]
  )

  const openContact = useCallback(() => navigate("#contact"), [])
  const closeContact = useCallback(() => navigate(""), [])

  return (
    <>
      <Layout>
        <Jumbotron onClick={openContact} />
        <Adress />
        <Highlight />
        <Container variant="text">
          <Text as="p" variant="p">
            Fort du succès du projet multi-primé Marseille by PHA5E, qui propose
            une cartographie de la ville en 3D et nombre de ses acteurs
            économiques et culturels, le studio PHA5E dévoilera durant cette
            soirée, trois nouvelles expériences digitales.
            <br />
            <br />
            Mêlant technologie et design, les installations choisies
            s’inscrivent dans la continuité du site en proposant aux
            utilisateurs, des expériences ludiques, artistiques et impactantes
            pour mettre en lumière Marseille.
          </Text>
          <Text as="p" variant="p">
            Les installations présentées ont vocation à faire émerger une
            émotion aux utilisateurs. Nous sommes convaincus que l’impact d’une
            marque passe par l’émotion qu’elle créé chez l’utilisateur.
            <br />
            <br />
            Marseille représente le terrain de jeu du studio PHA5E pour
            illustrer son savoir-faire en matière de création d’expérience.
          </Text>
        </Container>
        <Directors />
      </Layout>
      <Modal active={isContact} onClose={closeContact}>
        <FormContact />
      </Modal>
    </>
  )
}

export default Home
