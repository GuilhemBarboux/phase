import React from "react"
import { withPrefix } from "gatsby"
import WrapRoot from "./src/WrapRoot"
import WrapPage from "./src/WrapPage"

export const wrapRootElement = ({ element, props }) => {
  return <WrapRoot {...props}>{element}</WrapRoot>
}

export const wrapPageElement = ({ element, props }) => {
  return <WrapPage {...props}>{element}</WrapPage>
}

export const onRenderBody = ({ setHeadComponents, setHtmlAttributes }) => {
  setHeadComponents([
    <link
      key="druk-wide-bold"
      rel="preload"
      href={withPrefix("/fonts/druk-wide/druk-wide-bold-webfont.woff2")}
      as="font"
      type="font/woff2"
      crossOrigin="anonymous"
    />,
    <link
      key="area-semibold"
      rel="preload"
      href={withPrefix("/fonts/area/AreaNormal-Semibold.woff2")}
      as="font"
      type="font/woff2"
      crossOrigin="anonymous"
    />,
    <link
      key="area-extrablack"
      rel="preload"
      href={withPrefix("/fonts/area/AreaNormal-Extrablack.woff2")}
      as="font"
      type="font/woff2"
      crossOrigin="anonymous"
    />,
    <link
      key="fontfaces"
      rel="stylesheet"
      href={withPrefix("/fonts/druk-wide/stylesheet.css")}
    />,
    <link
      key="fontfaces"
      rel="stylesheet"
      href={withPrefix("/fonts/area/stylesheet.css")}
    />,
  ])
}
